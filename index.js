const express = require('express');
const cors = require('cors');
const { dbConnection } = require('./db/config');
const { path } = require('path');

require('dotenv').config();


//Crear servidor/aplicacion de express
const app = express();

//Base de datos 
dbConnection();

//Ditectorio publico
app.use( express.static( 'public' ))

//CORS
app.use( cors() );

//Lectura y parseo del body
app.use( express.json() );

//Rutas
app.use( '/api/auth', require('./routes/auth' ));


app.get( '*' , (req, res) =>{
  res.sendFile(path.resolve(__dirname) , 'public/index.html')
});

//levantar app
app.listen( process.env.PORT , () => {
  console.log(`Servidor corriendo en el puerto ${ process.env.PORT }`)
})