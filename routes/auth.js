const { Router } = require('express');
const { check } = require('express-validator');
const  authController  = require('../controllers/auth');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();

router.post( '/registrar',[
  check('email', 'El email es obligatorio.').isEmail(),
  check('password', 'La contraseña es obligatoria.').isLength( { min : 6 }),
  // check('password', 'La contraseña debe incluir al menos ocho caracteres,  una minuscula, una mayuscula, un numero y un caracter especial.').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/, "i"),
  check('nombre', 'El nombre es obligatorio.').notEmpty(),
  validarCampos
], authController.crearUsuario )

router.post('/login',[
  check('email', 'El email es obligatorio.').isEmail(),
  check('password', 'La contraseña es obligatoria.').isLength( { min : 6 }),
  validarCampos
], authController.loginUsuario)


router.get('/renew', validarJWT ,authController.revalidarToken)


module.exports = router;