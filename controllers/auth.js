
const Usuario = require('../models/Usuario');
const bcrypt = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt')
module.exports  = {

  crearUsuario : async(req, res) => {

    const { nombre, email, password } = req.body;
    
    try{
    //Verificar email
      const usuario = await Usuario.findOne({ email });

      if( usuario ){
        return res.status(400).json({ 
          ok : false,
          msg : 'El email ya se encuentra en uso.'
        })
      }

      //Crear usuario con el modelo de
      let dbUser = new Usuario( req.body );

      //Haashear contraseña
      const salt = bcrypt.genSaltSync();
      dbUser.password = bcrypt.hashSync( password, salt );

      //Generar JWT
      const token = await generarJWT( dbUser.id, nombre )


      //crear usuario de DB
      dbUser.save();

      //Generar respuesta exitosa
      return res.status(201).json({
        ok:true,
        ui: dbUser.id,
        nombre,
        email:dbUser.email,
        token
      })

    }catch(e){
      console.log(e);
      res.status(500).json({
        ok: false,
        msg: 'Por favor hable con el administrador.'
      });
    }



  },
  
  loginUsuario : async(req, res) => {
    const {  email, password } = req.body;


    try{
      const dbUser = await Usuario.findOne({ email });

      if(!dbUser) {
        return res.status(400).json({
          ok: false,
          msg: 'Credenciales no validas.'
        })
      }
      
      //confirmar match
      const validPassword = bcrypt.compareSync( password, dbUser.password );
      
      if(!validPassword) {
        return res.status(400).json({
          ok: false,
          msg: 'Credenciales no validas.'
        })
      }

      //generar jwt
      const token = await generarJWT( dbUser.id, dbUser.nombre );

      res.json({
        ok: true,
        uid: dbUser.id,
        nombre : dbUser.nombre,
        email : dbUser.email,
        token
      })

    }catch(e){
      console.log(e);
      res.status(500).json({
        ok: false,
        msg: 'Hable con el administrador.'
      })
    }

  },
  
  revalidarToken : async(req, res) => {
    const { uid, nombre } = req;

    const dbUser = await Usuario.findById( uid );

    const token = await generarJWT( uid, nombre );
    res.status(200).json({
      ok:true,
      uid, 
      nombre,
      email: dbUser.email,
      token
    })
  }


}
