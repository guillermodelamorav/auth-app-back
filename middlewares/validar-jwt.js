
const validarJWT =( req, res, next ) => {
const jwt = require('jsonwebtoken')

  const token = req.header('x-token');
  if(!token) {
    return res.status(401).json({
      ok: false,
      msg: 'error en el token'
    })
  }

  try{

    const { uid, nombre } = jwt.verify ( token, process.env.SECRET_JWT_SEED );

    req.uid = uid;
    req.nombre = nombre;

    next();

  }catch(e){
    return res.status(500).json({
      ok: false,
      msg: 'Token no valido'
    })
  }
}

module.exports = {
  validarJWT
}